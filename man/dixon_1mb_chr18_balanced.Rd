% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/HiC_functions.R
\docType{data}
\name{dixon_1mb_chr18_balanced}
\alias{dixon_1mb_chr18_balanced}
\title{1MB sample data}
\description{
This is data from Dixon et al. 2012 binned at 1mb bin size, and subsetted to just include chromosome 18
}
\examples{
data(dixon_1mb_chr18_balanced)

}
